<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders;


final class HeadersFactory implements \Mdfx\SecurityHeaders\DI\IHeadersFactory
{

	private Configuration $configuration;

	public function __construct(Configuration $configuration)
	{
		$this->configuration = $configuration;
	}

	/**
	 * @return \Mdfx\SecurityHeaders\Headers\IHeader[]
	 */
	public function getHeaders(): array
	{
		$headers = [
			new \Mdfx\SecurityHeaders\Headers\ContentTypeOptions\Header(),
			new \Mdfx\SecurityHeaders\Headers\XssProtection\Header(),
			new \Mdfx\SecurityHeaders\Headers\ReferrerPolicy\Header(\Mdfx\SecurityHeaders\Headers\ReferrerPolicy\Header::NO_REFERRER_WHEN_DOWNGRADE),
			new \Mdfx\SecurityHeaders\Headers\XPermittedCrossDomainPolicies\Header(),
			new \Mdfx\SecurityHeaders\Headers\ContentSecurityPolicy\Header(),
			new \Mdfx\SecurityHeaders\Headers\ExpectCt\Header(),
			new \Mdfx\SecurityHeaders\Headers\FeaturePolicy\Header(),
		];

		if ($this->configuration->isUseStrictTransportSecurity()) {
			$headers[] = new \Mdfx\SecurityHeaders\Headers\StrictTransportSecurityPolicy\Header();
		}

		return $headers;
	}

}
