<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders;

class Configuration
{

	private bool $useStrictTransportSecurity;

	public function __construct(
		bool $useStrictTransportSecurity
	)
	{
		$this->useStrictTransportSecurity = $useStrictTransportSecurity;
	}

	public function isUseStrictTransportSecurity(): bool
	{
		return $this->useStrictTransportSecurity;
	}

}
