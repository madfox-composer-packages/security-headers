<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\DI;

final class Extension extends \Nette\DI\CompilerExtension
{

	/**
	 * @var class-string
	 */
	private string $presenterHook = HeadersSetup::class;


	public function loadConfiguration()
	{
		parent::loadConfiguration();

		$containerBuilder = $this->getContainerBuilder();

		$config = (array) $this->getConfig();

		if (isset($config['presenterHook'])) {
			$this->presenterHook = $config['presenterHook'];
		}

		$presenterHookReflection = new \ReflectionClass($this->presenterHook);

		$containerBuilder
			->addDefinition($this->prefix('configuration'))
			->setFactory(\Mdfx\SecurityHeaders\Configuration::class)
			->setArguments($config)
		;

		if ( ! $presenterHookReflection->implementsInterface(IOnPresenterListener::class)) {
			throw new \RuntimeException(\sprintf('Hook presenteru musí implementovat rozhraní "%s"', IOnPresenterListener::class));
		}
	}


	public function beforeCompile()
	{
		$containerBuilder = $this->getContainerBuilder();

		$presenterHook = $containerBuilder
			->addDefinition($this->prefix('presenterHook'))
			->setFactory($this->presenterHook)
		;

		$applicationType = $containerBuilder->getByType(\Nette\Application\Application::class);

		/** @var \Nette\DI\Definitions\FactoryDefinition $application */
		$application = $containerBuilder->getDefinition($applicationType);
		$application->addSetup('?->onPresenter[] = ?', ['@self', [$presenterHook, 'onPresenter']]);

		$containerBuilder
			->addDefinition('headersFactory')
			->setFactory(\Mdfx\SecurityHeaders\HeadersFactory::class)
		;
	}


	public function getConfigSchema(): \Nette\Schema\Schema
	{
		return \Nette\Schema\Expect::structure([
			'useStrictTransportSecurity' => \Nette\Schema\Expect::bool(FALSE)->required(),
		]);
	}

}
