<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\DI;

interface IHeadersFactory
{

	/**
	 * @return array|\Mdfx\SecurityHeaders\Headers\ContentSecurityPolicy\Header[]
	 */
	public function getHeaders(): array;

}
