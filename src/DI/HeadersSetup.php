<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\DI;

final class HeadersSetup implements IOnPresenterListener
{

	/**
	 * @var IHeadersFactory
	 */
	private $factory;

	/**
	 * @var \Nette\Http\IResponse
	 */
	private $response;


	public function __construct(IHeadersFactory $factory, \Nette\Http\IResponse $response)
	{
		$this->factory = $factory;
		$this->response = $response;
	}


	public function onPresenter(\Nette\Application\Application $application, \Nette\Application\IPresenter $presenter): void
	{
		if (\PHP_SAPI === 'cli') {
			return;
		}

		foreach ($this->factory->getHeaders() as $header) {
			if (\array_key_exists($header->getName(), $this->response->getHeaders())) {
				continue;
			}

			$this->response->addHeader($header->getName(), $header->getValue());
		}

		$this->response->deleteHeader('X-Powered-By');
	}
}
