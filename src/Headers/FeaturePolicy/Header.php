<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\Headers\FeaturePolicy;

final class Header implements \Mdfx\SecurityHeaders\Headers\IHeader
{

	public function getName(): string
	{
		return 'Feature-Policy';
	}


	public function getValue(): string
	{
		return "camera 'none'; fullscreen 'self'; geolocation *; microphone 'none'";
	}

}
