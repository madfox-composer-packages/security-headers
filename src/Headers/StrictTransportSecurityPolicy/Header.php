<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\Headers\StrictTransportSecurityPolicy;

final class Header implements \Mdfx\SecurityHeaders\Headers\IHeader
{

	public function getName(): string
	{
		return 'Strict-Transport-Security';
	}


	public function getValue(): string
	{
		return 'max-age=31536000; includeSubDomains; preload';
	}

}
