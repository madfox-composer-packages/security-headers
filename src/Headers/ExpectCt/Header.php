<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\Headers\ExpectCt;

final class Header implements \Mdfx\SecurityHeaders\Headers\IHeader
{

	public function getName(): string
	{
		return 'Expect-CT';
	}


	public function getValue(): string
	{
		return 'enforce, max-age=31536000';
	}

}
