<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\Headers\XPermittedCrossDomainPolicies;

final class Header implements \Mdfx\SecurityHeaders\Headers\IHeader
{

	public function getName(): string
	{
		return 'X-Permitted-Cross-Domain-Policies';
	}


	public function getValue(): string
	{
		return 'none';
	}

}
