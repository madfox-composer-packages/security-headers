<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\Headers\ContentSecurityPolicy;

final class Header implements \Mdfx\SecurityHeaders\Headers\IHeader
{

	public function getName(): string
	{
		return 'Content-Security-Policy';
	}


	public function getValue(): string
	{
		return "default-src * data:; script-src https: 'unsafe-inline' 'unsafe-eval'; style-src https: 'unsafe-inline'";
	}

}
