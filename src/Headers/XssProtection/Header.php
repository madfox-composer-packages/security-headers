<?php declare(strict_types = 1);

namespace Mdfx\SecurityHeaders\Headers\XssProtection;

final class Header implements \Mdfx\SecurityHeaders\Headers\IHeader
{

	public function getName(): string
	{
		return 'X-XSS-Protection';
	}


	public function getValue(): string
	{
		return '1; mode=block';
	}

}
